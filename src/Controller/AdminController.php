<?php

namespace Drupal\freely_contest\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\freely_contest\Service\IStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminController extends ControllerBase
{

    /**
     * @var IStorage
     */
    protected $storage;

    /**
     * ContestForm constructor.
     * @param IStorage $storage
     */
    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param ContainerInterface $container
     * @return static
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('freely_contest.storage'));
    }

    /**
     * @return array
     */
    public function index()
    {
        return array(
            '#type' => 'markup',
            '#markup' => '<h1>' . t('List of Freely Contest submissions') . '</h1>' . $this->listSubmissions(),
        );
    }

    /**
     * @return string
     */
    protected function listSubmissions()
    {
        $submissions = $this->storage->findAll();
        if (empty($submissions)) {
            return 'Nothing has been submitted';
        }

        $result = '<table><tr><th>' . t('Name') . '</th><th>' . t('Email') . '</th><th>' . t('Code') . '</th></tr>';
        foreach ($submissions as $submission) {
            $result .= '<tr><td>' . htmlspecialchars($submission['name'], ENT_QUOTES, 'UTF-8') . '</td>';
            $result .= '<td>' . htmlspecialchars($submission['email'], ENT_QUOTES, 'UTF-8') . '</td>';
            $result .= '<td>' . htmlspecialchars($submission['code'], ENT_QUOTES, 'UTF-8') . '</td></tr>';
        }
        $result .= '</table>';

        return $result;
    }

}