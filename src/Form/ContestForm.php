<?php

namespace Drupal\freely_contest\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\freely_contest\Service\ICounter;
use Drupal\freely_contest\Service\ILogger;
use Drupal\freely_contest\Service\IStorage;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContestForm extends FormBase
{

    /**
     * @var ILogger
     */
    protected $logger;

    /**
     * @var IStorage
     */
    protected $storage;

    /**
     * @var ICounter
     */
    protected $counter;

    /**
     * ContestForm constructor.
     * @param ILogger $logger
     * @param IStorage $storage
     * @param ICounter $counter
     */
    public function __construct(ILogger $logger, IStorage $storage, ICounter $counter)
    {
        $this->logger = $logger;
        $this->storage = $storage;
        $this->counter = $counter;
    }

    /**
     * @param ContainerInterface $container
     * @return static
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('freely_contest.logger'),
            $container->get('freely_contest.storage'),
            $container->get('freely_contest.counter')
        );
    }

    /**
     * @return string
     */
    public function getFormId()
    {
        return strtolower(str_replace('\\', '_', get_called_class()));
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form = [
            'name' => [
                '#type' => 'textfield',
                '#title' => $this->t('Name'),
            ],
            'email' => [
                '#type' => 'email',
                '#title' => $this->t('Email'),
            ],
            'agreement' => [
                '#type' => 'checkbox',
                '#title' => $this->t('I agree to the processing of my personal data for marketing purposes'),
            ],
            'code' => [
                '#type' => 'textfield',
                '#title' => $this->t('Code'),
            ],
            'actions' => [
                'submit' => [
                    '#type' => 'submit',
                    '#value' => $this->t('Submit'),
                    '#button_type' => 'primary',
                ]
            ],
        ];

        return $form;
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $name = trim($form_state->getValue('name'));
        if (empty($name)) {
            $form_state->setErrorByName('name', $this->t('The name is required.'));
        }

        if (strlen($name) > 60) {
            $form_state->setErrorByName('name', $this->t('The name is longer than 60 characters.'));
        }

        if (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
            $form_state->setErrorByName('email', $this->t('The email is not valid email address.'));
        }

        if (!$form_state->getValue('agreement')) {
            $form_state->setErrorByName('agreement', $this->t('The agreement is required.'));
        }

        $code = trim($form_state->getValue('code'));
        if (empty($code)) {
            $form_state->setErrorByName('code', $this->t('The code is required.'));
        } elseif (strlen($code) > 60) {
            $form_state->setErrorByName('code', $this->t('The code is longer than 60 characters.'));
        } elseif ($this->counter->count('code', $code) > 0) {
            $form_state->setErrorByName('code', $this->t('The code is already in DB.'));
        }
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        try {
            $this->storage->save([
                'name' => $form_state->getValue('name'),
                'email' => $form_state->getValue('email'),
                'code' => $form_state->getValue('code'),
            ]);

            $this->logger->log($this->t('User with email @email has submitted @count code(s)', [
                '@email' => $form_state->getValue('email'),
                '@count' => $this->counter->count('email', $form_state->getValue('email'))
            ]));
        } catch (Exception $e) {
            $this->logger->fail($e->getMessage());
        }
    }

}