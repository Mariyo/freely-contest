<?php

namespace Drupal\freely_contest\Service;

use Drupal\Core\Database\Connection;

class DatabaseCounterService implements ICounter
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * DatabaseStorageService constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $column
     * @param string $value
     * @return int
     */
    public function count(string $column, string $value): int
    {

        return $this
            ->connection
            ->select('freely_contest_submissions')
            ->condition($column, $value)
            ->countQuery()
            ->execute()
            ->fetchField();

        $query = $this
            ->connection
            ->select('freely_contest_submissions')
            ->countQuery()
            ->condition($column, $value, '=');

        return $query->execute()->fetchField();
    }

}