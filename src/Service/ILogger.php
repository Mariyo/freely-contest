<?php

namespace Drupal\freely_contest\Service;

interface ILogger
{

    /**
     * @param string $message
     * @param string|null $type
     */
    public function log(string $message, string $type = null): void;

    /**
     * @param string $message
     */
    public function fail(string $message): void;

}