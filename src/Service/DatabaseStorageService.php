<?php

namespace Drupal\freely_contest\Service;

use Drupal\Core\Database\Connection;
use Exception;
use PDO;

class DatabaseStorageService implements IStorage
{

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * DatabaseStorageService constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $values
     * @return bool
     * @throws Exception
     */
    public function save(array $values): bool
    {
        $this
            ->connection
            ->insert('freely_contest_submissions')
            ->fields($values)
            ->execute();

        return true;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this
            ->connection
            ->query('SELECT * FROM freely_contest_submissions')
            ->fetchAll(PDO::FETCH_ASSOC);
    }

}