<?php

namespace Drupal\freely_contest\Service;

interface IStorage
{

    /**
     * @param array $values
     * @return bool
     */
    public function save(array $values): bool;

    /**
     * @return array
     */
    public function findAll(): array;

}