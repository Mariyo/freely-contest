<?php

namespace Drupal\freely_contest\Service;

interface ICounter
{

    /**
     * @param string $column
     * @param string $value
     * @return int
     */
    public function count(string $column, string $value): int;

}