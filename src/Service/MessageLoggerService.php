<?php

namespace Drupal\freely_contest\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class MessageLoggerService implements ILogger
{

    use StringTranslationTrait;

    /**
     * @var ICounter
     */
    protected $counter;

    /**
     * MessageLoggerService constructor.
     * @param ICounter $counter
     */
    public function __construct(ICounter $counter)
    {
        $this->counter = $counter;
    }

    /**
     * @param string $message
     * @param string $type
     */
    public function log(string $message, string $type = null): void
    {
        drupal_set_message($message, $type === null ? 'status' : $type);
    }

    /**
     * @param string $message
     */
    public function fail(string $message): void
    {
        $this->log($message, 'error');
    }

}