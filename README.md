# Tvorba custom modulu pro Drupal 8

Ve vsech pripadech se prosim snaz pouzivat standardni pristup pouzivany v Drupalu 8.
Dokumentace https://www.drupal.org/docs/8 je celkem rozsahla, ale najdes tam vetsinou vse co
potrebujes vcetne prikladu.

Veskery vyvoj modulu verzuj v GITu. Az bude hotovo, tak ti vytvorime repozitar v nasem gitlabu,
kam hotovy modul muzes nahrat.

Vytvor custom modul s nazvem freely_contest,​ ktery bude implementovat nasledujici:
1. V kodu​ bude implementovany formular, ktery bude obsahovat nasledujici pole
	1.1. Jmeno
	1.2. E-mail
	1.3. Souhlas se zpracovanim udaju
	1.4. Unikatni kod
	1.5. Tlaciko pro odeslani
2. Formular bude dostupny na url /soutez
3. Odeslane data se Budou ukladat to DB tabulky
	3.1. Samotnou DB tabulku vytvorit prosim v ramci modulu (tedy ne​ naklikanim pres phpmyadmina nebo tak neco)
	3.2. Konkretni podobu a formaty poli nechame na tobe
4. Formular bude validovat, jestli odeslany unikatni kod v DB jiz existuje a pokud ano, tak zobrazi validacni hlasku
5. Formular nebude ukladat zadna data, pokud uzivatel nesouhlasi se zpracovanim udaju (opet zobrazi validacni hlasku)
6. Vytvor service (https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dep endency-injection-in-drupal-8), ktera po uspesnem udeslani​ formulare vzdy uzivateli zobrazi, kolik odeslanych zaznamu uz ma uzivatel s danym mailem
	6.1. Tedy pokud by uzivatel s mailem marek.mechura@freely.agency mel v DB jiz dva zaznamy a odeslal by treti, tak by zobrazilo neco jako “Email marek.mechura@freely.agency jiz odeslal 3 kody”.
7. Na URL /admin/content/soutez​ by mel byt dostupny vypis odeslanych dat (idelane aby mel byt odkaz v admin menu - naznaceno na obrazku). Tento vypis by mel byt dostupny jen pro prihlaseneho​ uzivatele.